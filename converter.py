# -*- coding: utf-8 -*-
"""
Contains the movie converter class calling ffmpeg via its methods.
"""
import logging
from pathlib import Path
import subprocess
import time
from PyQt6.QtCore import QObject,QRunnable,pyqtSignal,QThread,QRegularExpression,Qt
from setup import DependencyChecker
try:
    from tqdm import tqdm
    import ffmpeg
except ModuleNotFoundError:
    DependencyChecker().check_and_install_modules()
    from tqdm import tqdm
    import ffmpeg

class MovieConverter:
    """
    Handles the conversion of the media files by calling ffmpeg via its python module.
    """
    def __init__(self, app, *args, **kwargs):
        super(MovieConverter, self).__init__(*args, **kwargs)
        self.app = app
        self.logger = logging.getLogger('trackback')
        self.customize_logger(args)
        self.framecount = ""
        self.mov = "None"
        self._ffmpeg_args = []
        self.output_file = "None"
        self.fpg = ffmpeg
        self.threads = []
        self.framecount = ""
        self.set_cmd_dict()
        self.process = FfmpegProcess(self)
        self.ffmpeg_loglevel="verbose"
        self.command = list(self.cmd_dict.values())
        self.new_command = []
        self.reset_ffmpeg_bin_path()
        self.multi_images_mode = False
        self._can_get_duration = False
        self.temp_title = "tmp_list.txt"
        self.output_img_sequence = False
        self.queue = []

    def set_cmd_dict(self):
        self.cmd_dict = {}

    def customize_logger(self,args):
        logger_handler = logging.StreamHandler()
        logger_handler.setFormatter(CustomFormatter())
        self.logger.addHandler(logger_handler)

    def reset_ffmpeg_bin_path(self):
        self.cmd_dict['cmd'] = "ffmpeg"
        if self.app.bin_path and 'ffmpeg' in [Path(x).stem for x in list(Path(self.app.bin_path).glob("*"))]:
            self.cmd_dict['cmd'] = f"{Path(self.app.bin_path).joinpath('ffmpeg')}"
        self._ffmpeg_args = self.command = self.new_command = [self.cmd_dict['cmd']]

    def convert(self, list_of_files=None):
        """
        Tests if the list_of_files contains files to convert and calls the conversion method.

        Args:
            list_of_files: list of file paths [str, str...]
        """
        if list_of_files != None :
            self.queue = self.gen_list_of_files(list_of_files)
            self.process_next_file()

    def process_next_file(self):
        """Processes the next file from the generator."""
        try:
            next_file = next(self.queue)
            if not self.multi_images_mode :
                self.mov = next_file
                #self.logger.error(self.mov)
            self.refresh_cmd_args()
            self.run_ffmpeg()

        except StopIteration:
            # All files have been processed
            self.logger.error("All files processed!")

    def gen_list_of_files(self,_list):
        for file in _list:
            yield file

    def refresh_cmd_args(self):
        #self.reset_ffmpeg_bin_path()
        self.multi_images_mode = False
        if self.app.op.output_widget.vid_from_img.isChecked() and len(self.app.list_of_files)>1:
            self.multi_images_mode = True
            #self.logger.error("multi_images_mode set to true")
        #self._output_filepath = str(self.command[-1])
        self.percentage = 0
        self.v_filters = []
        self.a_filters = []
        self.cmd_dict['main_arg'] = self.mov
        self.cmd_dict['end_arg'] = self.set_output()
        self.reset_ffmpeg_bin_path()

    def insert_pre_stills(self):
        if self.app.op.output_widget.vid_from_img.isChecked() and len(self.app.list_of_files)<2:
            self._ffmpeg_args.insert(2, "-loop")
            self._ffmpeg_args.insert(3, "1")
            self.arg_index += 2
        else:
            still_cmds = ["-f","concat","-safe","0","-i", self.temp_title,"-t",self.app.op.output_widget.still_vid_duration.text()]
            self._ffmpeg_args += still_cmds
            self.arg_index += len(still_cmds)
            self.make_temp_list()

    def check_audio(self,stream):
        pass

    def get_frame_count(self,filename):
        probe = self.fpg.probe(filename)
        frames = None
        try:
            video_info = next(s for s in probe['streams'] if s['codec_type'] == 'video')
            frames = probe['streams'][0]['nb_frames']
            #print(video_info)
        except KeyError:
            #print('using duration_ts')
            frames = probe['streams'][0]['duration_ts']

        #print(f"Shot has, {frames}, frames")
        return frames

    def set_output(self):
        """
        Set output according to current options
        """
        if self.app.op.output_widget.show_count.isChecked():
            self.framecount = f"_{self.get_frame_count(self.mov)}"
        prefix = self.app.op.output_widget.out_prefix.text() if self.app.op.output_widget.use_prefix.checkState() == Qt.CheckState.Checked  else ""
        dest = str(Path(self.mov).parent)
        suffix = self.app.op.output_widget.out_suffix.text() if self.app.op.output_widget.use_suffix.checkState() == Qt.CheckState.Checked  else ""
        ext = self.app.op.output_widget.enum_out_ext.currentText()
        keep_origin = self.app.op.output_widget.keep_origin_folder.isChecked()
        frames = ""
        if not Path(dest).exists():
                print("ERROR PATH! ",dest)
        output = f"{dest}/{prefix}sequence{frames}{suffix}{ext}"
        if not keep_origin :
            dest = self.app.op.output_widget.dest.text()
        overwrite = self.app.op.output_widget.overwrite.isChecked()
        if Path(self.mov).is_file and "%" not in self.mov:
            output = f"{dest}/{prefix}{Path(self.mov).stem}{self.framecount}{suffix}{ext}"
        if self.output_img_sequence:
            output = f"{dest}/{prefix}{self.framecount}{suffix}_img_%04d{ext}"
        return output

    def after_run(self):
        self.app.pbar.hide()
        self.app.uncheck_all()

    def run_ffmpeg(self):
        self.app.pbar.show()
        cmd_list = self.prep_run()
        #self.logger.error(cmd_list)
        if not self.app.op.output_widget.disable_tdqm.isChecked():
            self.process.run_with_tdqm(cmd_list)
        else :
            self.process.run(cmd_list)
        self.app.uncheck_all()

    def insert_audio_source(self):
        if self.app.op.audio_widget.replace_audio.isChecked() and not self.app.op.audio_widget.use_og_audio.isChecked() and self.app.op.audio_widget.audio_group.isChecked():
            self._ffmpeg_args.insert(self.arg_index, "-i")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, self.app.op.audio_widget.audio_file_substitute.text())
            self.arg_index += 1

    def set_arate(self):
        if not self.app.op.audio_widget.use_og_audio.isChecked():
            self._ffmpeg_args.insert(self.arg_index, "-ar")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, self.app.op.audio_widget.sample_rate.text())
            self.arg_index += 1
            if "libmp3lame" in self.app.op.audio_widget.enum_audio_codecs.currentText() :
                self._ffmpeg_args.insert(self.arg_index, "-b:a")
                self.arg_index += 1
                self._ffmpeg_args.insert(self.arg_index, f"{self.app.op.audio_widget.a_mp3_bit_rate.text()}k")
                self.arg_index += 1

    def insert_wartemark(self):
        self._ffmpeg_args.insert(self.arg_index, "-i")
        self.arg_index += 1
        self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.watermark_file.text())
        self.arg_index += 1

    def add_vcodec(self):
        if self.app.op.video_widget.use_hwaccel.isChecked():
            self._ffmpeg_args.insert(2, "-hwaccel")
            self.arg_index += 1
            self._ffmpeg_args.insert(3, "cuvid")
            self.arg_index += 1

        if self.app.op.video_widget.video_group.isChecked():
            self._ffmpeg_args.insert(self.arg_index, "-c:v")
            self.arg_index += 1
            if self.app.op.video_widget.use_og_video.isChecked():
                self._ffmpeg_args.insert(self.arg_index, "copy")
                self.arg_index += 1
            else:
                self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.enum_video_codecs.currentText())
                self.arg_index += 1

                if self.app.op.video_widget.enum_video_codecs.currentText() in 'libx264':
                    self._ffmpeg_args.insert(self.arg_index, "-g")
                    self.arg_index += 1
                    self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.gop.text())
                    self.arg_index += 1
                    if self.app.op.video_widget.use_crf.isChecked():
                        self._ffmpeg_args.insert(self.arg_index, "-crf")
                        self.arg_index += 1
                        self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.crf.text())
                        self.arg_index += 1

                if self.app.op.output_widget.vid_from_img.isChecked():
                    self._ffmpeg_args.insert(self.arg_index,"-t")
                    self.arg_index += 1
                    self._ffmpeg_args.insert(self.arg_index,self.app.op.output_widget.still_vid_duration.text())
                    self.arg_index += 1

                if self.app.op.video_widget.compat_mode.isChecked():
                    self._ffmpeg_args.insert(self.arg_index, "-pix_fmt")
                    self.arg_index += 1
                    self._ffmpeg_args.insert(self.arg_index, "yuv420p")
                    self.arg_index += 1

                if self.app.op.video_widget.enum_video_codecs.currentText() in "gif":
                    self._ffmpeg_args.insert(self.arg_index, "-pix_fmt")
                    self.arg_index += 1
                    self._ffmpeg_args.insert(self.arg_index, "rgba")
                    self.arg_index += 1
        else:
            self._ffmpeg_args.insert(self.arg_index, "-vn")
            self.arg_index += 1

    def add_selectors(self):
        if self.app.op.video_widget.use_preset.isChecked():
            self._ffmpeg_args.insert(self.arg_index, "-preset")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.enum_preset.currentText())
            self.arg_index += 1

        if self.app.op.video_widget.use_tune.isChecked():
            self._ffmpeg_args.insert(self.arg_index, "-tune")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.enum_tunes.currentText())
            self.arg_index += 1

        if self.app.op.video_widget.use_profiles.isChecked():
            self._ffmpeg_args.insert(self.arg_index, "-profile")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.enum_profile.currentText())
            self.arg_index += 1

    def add_acodec(self):
        #TODO: refactor to use add_a_filters if more audio options get implemented
        if self.app.op.audio_widget.audio_group.isChecked():
            if self.app.op.audio_widget.reverse_audio.isChecked():
                self._ffmpeg_args.insert(self.arg_index, "-af")
                self.arg_index += 1
                self._ffmpeg_args.insert(self.arg_index, "areverse")
                self.arg_index += 1
            else:
                self._ffmpeg_args.insert(self.arg_index, "-c:a")
                self.arg_index += 1
                if self.app.op.audio_widget.use_og_audio.isChecked():
                    self._ffmpeg_args.insert(self.arg_index, "copy")
                else:
                    self._ffmpeg_args.insert(self.arg_index, self.app.op.audio_widget.enum_audio_codecs.currentText())
                self.arg_index += 1
                self.set_arate()
        else:
            self._ffmpeg_args.insert(self.arg_index, "-an")
            self.arg_index += 1

    def show_command(self):
        txt = ' '.join(self._ffmpeg_args)
        #self.logger.error(txt)
        self.app.cmd_displayed.setText(txt)
        """
        if not self.multi_images_mode:
            txt = ""
            for string in self._ffmpeg_args:
                if Path(string).parent.exists() and Path(string).parent != Path("."):
                    txt += f" '{string}'"
                else:
                    txt += f" {string}"
            self.app.cmd_displayed.setText(txt)
        else:
            #concatenated string nog good syntax for movie from stills mode - turned ON
            pass
        """

    def add_a_filters(self):
        self.a_filters.clear()
        if self.app.op.audio_widget.reverse_audio.isChecked():
            self.a_filters.append("areverse")
        if len(self.a_filters) > 0:
            self._ffmpeg_args.insert(self.arg_index, "-af")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, ", ".join(self.a_filters))
            self.arg_index += 1

    def add_v_filters(self):
        self.v_filters.clear()
        tmp_v_filters = []
        if self.app.op.video_widget.add_watermark.isChecked():
            if not self.app.op.video_widget.watermark_field.text() and self.app.op.video_widget.watermark_file.text():
                tmp_v_filters = [f"[0:v][1:v] overlay=0:0"]
            elif self.app.op.video_widget.watermark_field.text():
                tmp_v_filters = [f"[0:v]drawtext=text='{self.app.op.video_widget.watermark_field.text()}':fontcolor=white:fontsize=75:x=30:y=30"]
            if self.app.op.video_widget.hflip.isChecked():
                tmp_v_filters.append("hflip")
            if self.app.op.video_widget.vflip.isChecked():
                tmp_v_filters.append("vflip")
            if self.app.op.video_widget.reverse.isChecked():
                tmp_v_filters.append("reverse")
            if self.app.op.video_widget.use_scaler.isChecked():
                tmp_v_filters.append(f'scale={self.app.op.video_widget.scaler.text()}')
        if len(tmp_v_filters)>1:
            self._ffmpeg_args.insert(self.arg_index, "-filter_complex")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, ",".join(tmp_v_filters))
            self.arg_index += 1
        else :
            if self.app.op.video_widget.use_scaler.isChecked() :
                self.v_filters.append(f"scale={self.app.op.video_widget.scaler.text()}")
            if self.app.op.video_widget.hflip.isChecked():
                self.v_filters.append("hflip")
            if self.app.op.video_widget.vflip.isChecked():
                self.v_filters.append("vflip")
            if self.app.op.video_widget.reverse.isChecked():
                self.v_filters.append("reverse")
        if len(self.v_filters) > 0:
            self._ffmpeg_args.insert(self.arg_index, "-vf")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, ", ".join(self.v_filters))
            self.arg_index += 1
        if not self.app.op.video_widget.keep_fps.checkState() == Qt.CheckState.Checked :
            self._ffmpeg_args.insert(self.arg_index, "-r")
            self.arg_index += 1
            self._ffmpeg_args.insert(self.arg_index, self.app.op.video_widget.fps.text())
            self.arg_index += 1

    def concat_images_names(self):
        str_concat = ""
        for img in self.app.list_of_files :
            str_concat += f"file '{img}'\nduration {self.app.op.output_widget.frame_length.text()}\n"
        return str_concat

    def make_temp_list(self):
        filer = f"{Path(__file__).parent.joinpath(self.temp_title)}"
        with open(filer, 'w') as config:
            for img in self.app.list_of_files :
                config.write(f"file '{img}'\n")
                config.write(f"duration {self.app.op.output_widget.frame_length.text()}\n")

    def add_source(self):
        self._ffmpeg_args.insert(self.arg_index, "-i")
        self.arg_index += 1
        self._ffmpeg_args.insert(self.arg_index, self.mov)
        self.arg_index += 1

    def add_inputs(self):
        #self.arg_index = 8
        if self.multi_images_mode:
            self.insert_pre_stills()
        else:
            self.add_source()
        if self.app.op.video_widget.add_watermark.isChecked() and not self.app.op.video_widget.watermark_field.text():
            self.insert_wartemark()
        if self.app.op.audio_widget.audio_group.isChecked():
            self.insert_audio_source()

    def add_midargs(self):
        #self.add_a_filters()
        if not self.output_img_sequence:
            self.add_vcodec()
            self.add_acodec()
        self.add_v_filters()
        self.add_selectors()

    def get_output_logger(self):
        _p = Path(__file__).parent.joinpath('ffmpeg_output')
        if not(_p.exists() and _p.is_dir()):
            _p.mkdir(exist_ok=True)
        return f"{_p.joinpath('[lastoutput].txt')}"

    def prep_run(self, progress_handler=None, ffmpeg_output_file=None):
        progress_bar = None
        if self.app.op.output_widget.overwrite.isChecked():
            self._ffmpeg_args.insert(1, "-y")
        self.arg_index = 2
        self.add_inputs()
        self.add_midargs()
        self._ffmpeg_args.append(self.set_output())
        self.show_command()
        return self._ffmpeg_args

class FfmpegProcess:
    def __init__(self,app):
        self.app = app
        self._ffmpeg_args = self.app._ffmpeg_args
        #self._output_filepath = self.app._output_filepath
        #self._dir_files = self.app._dir_files
        #self._duration_secs = self.app._duration_secs
        self.percentage = 0
        self.ffmpeg_worker = None

    def start_ffmpeg(self):
        self.ffmpeg_worker = FFmpegWorker(self.app)
        self.ffmpeg_worker.progress_signal.connect(self.update_progress)
        self.ffmpeg_worker.finished.connect(self.ffmpeg_finished)
        self.app.app.btn_convert.setEnabled(False)
        self.app.app.pbar.setValue(0)
        self.ffmpeg_worker.start()

    def update_progress(self, value):
        self.app.app.pbar.setValue(value)

    def ffmpeg_finished(self):
        self.app.app.btn_convert.setEnabled(True)
        self.app.app.pbar.setValue(100)
        self.app.app.pbar.hide()
        if not self.app.multi_images_mode :
            self.app.process_next_file()

    def run(self,args):
        process = subprocess.Popen(args)

    def run_with_tdqm(self, progress_handler=None, ffmpeg_output_file=None, process_complete_handler=None):
        self.start_ffmpeg()

class CustomFormatter(logging.Formatter):
    """Custom logging formater class.

        >>> Used to display the messages from logging module in a custom format.

        Args:
            None

        Returns:
            None.
    """
    def format(self, record):
        # Include file path and line number in the log message
        record.msg = f"{record.filename}:{record.lineno} - {record.msg}"
        return super().format(record)

class FFmpegWorker(QThread):
    progress_signal = pyqtSignal(int)  # pyqtSignal to send progress to the GUI

    def __init__(self, app, parent=None):
        super().__init__(parent)
        self.running = False
        self.app = app
        self.ffmpeg_args = self.app._ffmpeg_args

    def run(self):
        self.running = True
        #self.app.logger.error("run")
        """
        # FFmpeg command with progress output
        command = [
            "ffmpeg",
            "-i", self.input_file,
            "-c:v", "libx264",
            "-progress", "pipe:1",
            "-nostats",
            self.output_file
        ]
        """
        self.ffmpeg_args.insert(-1,"-progress")
        self.ffmpeg_args.insert(-1,"pipe:1")
        self.ffmpeg_args.insert(-1,"-nostats")
        #self.app.logger.error((" ").join(self.ffmpeg_args))
        # Launch FFmpeg process
        process = subprocess.Popen(self.ffmpeg_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

        # Total duration (in seconds)
        total_duration = None
        progress_bar = None

        for line in process.stdout:
            if not self.running:
                #self.logger.error("terminated")
                process.terminate()
                break

            # Extract duration from FFmpeg output
            if total_duration is None:
                regex = QRegularExpression(r"Duration: (\d+):(\d+):(\d+\.\d+)")
                match = regex.match(line)
                if match.hasMatch():
                    hours, minutes, seconds = map(float, [float(match.captured(i+1)) for i in range(3)])
                    total_duration = int(hours * 3600 + minutes * 60 + seconds)
                    progress_bar = tqdm(total=total_duration, unit="s", disable=True)

            # Extract progress time
            regex = QRegularExpression(r"time=(\d+):(\d+):(\d+\.\d+)")
            match = regex.match(line)
            if match.hasMatch() and total_duration:
                hours, minutes, seconds = map(float, [float(match.captured(i+1)) for i in range(3)])
                elapsed_time = int(hours * 3600 + minutes * 60 + seconds)

                # Update progress
                if progress_bar:
                    progress_bar.n = elapsed_time
                    progress_bar.refresh()

                # Send progress to GUI
                percentage = int((elapsed_time / total_duration) * 100)
                self.progress_signal.emit(percentage)

        process.wait()

        # Close the tqdm progress bar
        if progress_bar:
            progress_bar.close()

