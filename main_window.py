# -*- coding: utf-8 -*-
"""
Main User interface of the app.
"""

from PyQt6.QtWidgets import QWidget, QFileDialog, QGroupBox, QMainWindow, QPushButton, QVBoxLayout, QLabel, QMessageBox,QCheckBox,QComboBox,QLineEdit
from PyQt6.QtGui import QAction,QStandardItem, QIcon
from PyQt6.QtCore import QRect,QEvent,Qt,QRegularExpression
import ressources
import sys
from pathlib import Path
from ui import MainPanel
import json


class MainWindow(QMainWindow):
    """
    Main widget displaying the list of files to convert.
    """
    def __init__(self, application, parent=None):
        """
        Main method initializing the widget.

        Args:
            application: class instanciating this object.
            parent: parent class.
        """
        super(MainWindow, self).__init__(parent=parent)

        self.setWindowTitle('Encoder E-Medias')

        menu = self.menuBar()
        self.panel = MainPanel(self)
        self.exit_btn = QAction("&Exit", self)
        self.save_playlist_btn = QAction("&Save Playlist", self)
        self.load_playlist_btn = QAction("&Load Playlist", self)
        self.application = application
        self.help_btn = QAction("&Help", self)

        #self.addfolder_btn = QAction(QIcon(":/closed.png"), "&Add Folder", self)
        self.addfolder_btn = QAction("&Add Folder", self)
        self.addfile_btn = QAction("&Add File", self)
        self.del_btn = QAction("&Remove File", self)
        self.clear_btn = QAction("&Clear Queue", self)

        self.options_btn = QAction("&Options", self)
        self.bin_path_setter = QAction("&Ffmpeg Path", self)
        self.convert_btn = QAction("&Convert !", self)

        file_menu = menu.addMenu("&File")
        file_menu.addAction(self.addfolder_btn)
        file_menu.addAction(self.addfile_btn)
        file_menu.addAction(self.del_btn)
        file_menu.addAction(self.clear_btn)

        edit_menu = menu.addMenu("&Edit")
        edit_menu.addAction(self.options_btn)
        edit_menu.addAction(self.bin_path_setter)
        edit_menu.addAction(self.convert_btn)

        about_menu = menu.addMenu("&About")
        about_menu.addAction(self.help_btn)

        file_menu.addAction(self.save_playlist_btn)
        file_menu.addAction(self.load_playlist_btn)
        #file_menu.addSeparator()
        file_submenu = file_menu.addMenu("&Exit")
        file_submenu.addAction(self.exit_btn)
        self.options_btn.triggered.connect(self.call_options)
        self.addfolder_btn.triggered.connect(self.panel.showDialog)
        self.addfile_btn.triggered.connect(self.panel.add_a_file)
        self.del_btn.triggered.connect(self.panel.remove_file)
        self.clear_btn.triggered.connect(self.panel.clear_list)
        self.convert_btn.triggered.connect(self.panel.activated)
        self.bin_path_setter.triggered.connect(self.panel.set_ffmpeg_path)
        self.exit_btn.triggered.connect(self.exit_app)
        self.load_playlist_btn.triggered.connect(self.load_playlist)
        self.save_playlist_btn.triggered.connect(self.save_playlist)
        self.help_btn.triggered.connect(self.show_about)
        self.main_win = QVBoxLayout()
        self.main_win.addWidget(self.panel)
        self.setCentralWidget(self.panel)
        #self.setLayout(self.main_win)
        self.resize(760, 750)
        #self.load_preset()
        #QApplication.instance()
        #pyside_app
        self.application.pyside_app.installEventFilter(self)

    def eventFilter(self, obj, event):
        if event.type() == QEvent.Type.MouseButtonPress:
            mouse_pos = event.globalPosition()
            if not self.panel.preset_adder_frame.isHidden():
                if not self.panel.preset_adder_frame.geometry().contains(mouse_pos.toPoint()):
                    self.panel.preset_adder_frame.hide()
            if not self.panel.preset_remover_frame.isHidden():
                if not self.panel.preset_remover_frame.geometry().contains(mouse_pos.toPoint()):
                    self.panel.preset_remover_frame.hide()
            #geometry_tot = s_box.geometry().united(s_box.status_lister.view().window().geometry())
        return super().eventFilter(obj, event)

    def save_config(self):
        dico = {'ffmpeg_bin_folder':self.panel.bin_path}
        filer = f"{Path(__file__).parent.joinpath('config.json')}"
        with open(filer, 'w') as config:
            json.dump(dico, config,indent=4)

    def get_config(self):
        filer = Path(__file__).parent.joinpath('config.json')
        if filer.exists() and filer.is_file():
            with open(f"{filer}", 'r') as config:
                dico = json.load(config)
                self.panel.bin_path = dico['ffmpeg_bin_folder']
                self.panel.mc.reset_ffmpeg_bin_path()

    def call_options(self):
        self.panel.op.options_group.setChecked(not self.panel.op.options_group.isChecked())

    def exit_app(self):
        sys.exit()

    def save_playlist(self):
        dialog = QFileDialog()
        dialog.setDefaultSuffix(".pls")
        file = dialog.getSaveFileName(self, "Save playlist", "", "Playlist Files (*.pls)")[0]
        if ".pls" not in str(file):
            file += ".pls"
        print(self.panel.list_of_files)
        dico = dict(zip(range(len(self.panel.list_of_files)),self.panel.list_of_files))
        with open(file, 'w') as playlist:
            json.dump(dico, playlist,indent=4)

    def load_playlist(self):
        self.panel.list_of_files = []
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode.ExistingFile)
        dialog.setOption(QFileDialog.Option.ShowDirsOnly,False)
        dialog.exec()
        with open(dialog.selectedFiles()[0], 'r') as playlist:
            self.panel.add_files(list(json.load(playlist).values()))
    
    def set_watermark_url(self):
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode.ExistingFile)
        dialog.setOption(QFileDialog.Option.ShowDirsOnly,False)
        dialog.exec()
        with open(dialog.selectedFiles()[0], 'r') as ww:
            self.panel.op.video_widget.watermark_file.setText(dialog.selectedFiles()[0])

    def get_options_params(self):
        all_out = []
        all_aud = []
        all_vid = []
        params = [all_out,all_vid,all_aud]
        ops = [self.panel.op.output_widget,self.panel.op.video_widget,self.panel.op.audio_widget]
        wg = [QCheckBox,QComboBox,QLineEdit]
        for i,_cls in enumerate(ops) :
            #self.panel.mc.logger.error(f"{_cls.objectName()} ")
            for _w in wg :
                for sub_cls in _cls.findChildren(_w):
                    params[i].append(sub_cls)
            #self.panel.mc.logger.error(f"{params[i]} ")
        return params

    def set_widget_value(self,wg,val):
        if isinstance(wg,QCheckBox):
            wg.setChecked(val)
        elif isinstance(wg,QGroupBox):
            wg.setChecked(val)
        elif isinstance(wg,QComboBox):
            wg.setCurrentText(val)
        elif isinstance(wg,QLineEdit):
            wg.setText(val)

    def fetch_widget_value(self,wg):
        if isinstance(wg,QCheckBox):
            return wg.isChecked()
        elif isinstance(wg,QComboBox):
            return wg.currentText()
        elif isinstance(wg,QLineEdit):
            return wg.text()
        return None

    def pack_values(self):
        self.all_values = {"output_widget":{},"video_widget":{"video_group":self.panel.op.video_widget.video_group.isChecked()},"audio_widget":{"audio_group":self.panel.op.audio_widget.audio_group.isChecked()}}
        ctg = ["output_widget","video_widget","audio_widget"]
        all_params = self.get_options_params()
        #self.panel.mc.logger.error(f"dico: {all_params} ")
        for index,category in enumerate(all_params):
            for _cls in category :
                self.all_values[ctg[index]][_cls.objectName()] = self.fetch_widget_value(_cls)

    def save_preset(self,name):
        self.pack_values()
        filer = f"{Path(__file__).parent.joinpath('presets').joinpath('_'.join(name.split(' '))+'.json')}"
        with open(filer, 'w') as preset:
            json.dump(self.all_values, preset,indent=4)
        self.panel.fast_encode.model().clear()
        self.panel.fast_encode.addItems(self.panel.ut.get_fast_options())
        self.panel.preset_adder_frame.hide()

    def load_preset(self):
        dico = {'ffmpeg_bin_folder':self.panel.bin_path}
        filer = f"{Path(__file__).parent.joinpath('presets').joinpath(self.panel.fast_encode.currentText()+'.json')}"
        with open(filer, 'r') as preset:
            self.all_values = json.load(preset)
        self.apply_preset()

    def apply_preset(self):
        for ctg,lst in self.all_values.items() :
            #self.panel.mc.logger.error(ctg)
            _cls = getattr(self.panel.op,ctg)
            for k,v in lst.items():
                self.set_widget_value(getattr(_cls,k),v)

    def show_about(self):
        QMessageBox.information(self, "About","Written by Cosmin Planchon")
