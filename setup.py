import subprocess
import sys
from importlib.util import find_spec

class DependencyChecker():
    def __init__(self):
        self.required_modules = ["PyQt6", "ffmpeg", "tqdm", "pyseq"]
        
    def check_and_install_modules(self):
        """
        Checks for the specified modules and prompts the user to allow installation if any are missing.

        Args:
            modules (list of str): List of module names to check and potentially install.
        """
        missing_modules = [mod for mod in self.required_modules if not find_spec(mod)]

        if not missing_modules:
            print("All required modules are installed.")
            return

        print(f"The following modules are missing: {', '.join(missing_modules)}")
        user_input = input("Allow installation yes/no ? (default = y) : ").strip().lower()
        if not user_input or user_input.strip().lower() in ['yes','oui','o']:
            user_input = 'y'
        if user_input.strip().lower() != 'y':
            print("Installation canceled.")
            print(f"Please manually install the missing modules: {', '.join(missing_modules)}")
            return

        for module in missing_modules:
            try:
                print(f"Installing {module}...")
                subprocess.check_call([sys.executable, "-m", "pip", "install", module])
            except subprocess.CalledProcessError as e:
                print(f"Failed to install {module}. Error: {e}")

        print("Installation process complete.")
        print("Restart the application if needed.")
