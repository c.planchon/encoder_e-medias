# -*- coding: utf-8 -*-

class EasyDev:
    """
    Class used to identify attributes by hovering in the UI for easy debugging, activate by setting self.debug = True below.
    """
    def __init__(self, app):
        self.app = app
        self.debug = False

    def set_tooltips(self):
        aop = self.app.op.audio_widget
        vop = self.app.op.video_widget
        oop = self.app.op.output_widget
        mop = self.app.op
        aap = self.app
        #print(op.__dict__.keys())
        oop.disable_tdqm.setToolTip(f" {'disable_tdqm' if self.debug else 'Use this if the App crashes during conversion'}")
        aap.add_preset_btn.setToolTip(f" {'add_preset_btn' if self.debug else 'Write current config as new preset'}")
        aap.del_preset_btn.setToolTip(f" {'del_preset_btn' if self.debug else 'Delete current preset'}")
        oop.keep_origin_folder.setToolTip(f" {'keep_origin_folder' if self.debug else 'Write converted media in the same folder as the source file'}")
        oop.vid_from_img.setToolTip(f" {'vid_from_img' if self.debug else 'Enable to create a short from input images'}")
        oop.still_vid_duration.setToolTip(f" {'still_vid_duration' if self.debug else 'Duration of the short (in seconds)'}")
        oop.still_vid_lbl.setToolTip(f" {'still_vid_lbl' if self.debug else 'Length of the movie'}")
        vop.enum_quality.setToolTip(f" {'enum_quality' if self.debug else 'I forgot what this was for'}")
        vop.use_og_video.setToolTip(f" {'use_og_video' if self.debug else 'Keep original video codec'}")
        oop.enum_out_ext.setToolTip(f"{'enum_out_ext' if self.debug else 'extension to use for the encoded file '}")
        oop.out_ext_label.setToolTip(f"{'out_ext_label' if self.debug else 'extension to use for the encoded file '}")
        aop.enum_audio_codecs.setToolTip(f"{'enum_audio_codecs' if self.debug else 'Audio codec to use for the audio stream encoding '}")
        aop.reverse_audio.setToolTip(f"{'reverse_audio' if self.debug else 'Reverse Audio track'}")
        aop.audio_codec.setToolTip(f"{'enum_audio_codecs' if self.debug else 'Audio codec to use for the audio stream encoding '}")
        vop.enum_video_codecs.setToolTip(f"{'enum_video_codecs' if self.debug else 'Video codec to use for the video stream encoding '}")
        vop.codec_label.setToolTip(f"{'codec_label' if self.debug else 'Video codec to use for the video stream encoding '}")
        vop.qual_group.setToolTip(f"{'qual_group' if self.debug else 'Options of the video codec'}")
        #vop.layout_video.setToolTip(f"{'layout_video' if self.debug else 'layout_video options'}")
        vop.use_crf.setToolTip(f"{'use_crf' if self.debug else 'Set Constant Rate Factor for codec'}")
        vop.crf.setToolTip(f"{'crf' if self.debug else 'Constant Rate Factor'}")
        vop.use_hwaccel.setToolTip(f"{'use_hwaccel' if self.debug else 'Use GPU during encoding (Experimental)'}")
        vop.use_scaler.setToolTip(f"{'use_scaler' if self.debug else 'Rescale video file dimensions'}")

        vop.scaler.setToolTip(f"{'scaler' if self.debug else 'Video file dimensions target when rescalling'}")

        vop.compat_mode.setToolTip(f"{'compat_mode' if self.debug else 'Enable this to be compatible with old players versions'}")
        vop.enum_profile.setToolTip(f"{'enum_profile' if self.debug else 'Sets of features and capabilities for video compression and playback, trading compression efficiency vs. device compatibility'}")
        vop.enum_preset.setToolTip(f"{'enum_preset' if self.debug else 'trade-off between encoding speed and compression efficiency (file size and quality)'}")
        vop.use_preset.setToolTip(f"{'use_preset' if self.debug else 'Trade-off between encoding speed and compression efficiency (file size and quality)'}")
        vop.use_tune.setToolTip(f"{'use_tune' if self.debug else 'Optimize video encoding for specific content types to improve quality or compression efficiency based on the videos characteristics.'}")
        vop.enum_tunes.setToolTip(f"{'enum_tunes' if self.debug else 'Optimize video encoding for specific content types to improve quality or compression efficiency based on the videos characteristics.'}")
        vop.use_profiles.setToolTip(f"{'use_profiles' if self.debug else 'Sets of features and capabilities for video compression and playback, trading compression efficiency vs. device compatibility'}")


        aop.audio_group.setToolTip(f"{'audio_group' if self.debug else 'Process audio, uncheck this if you do not need audio '}")
        vop.gop.setToolTip(f"{'gop' if self.debug else 'Distance between two keyframes, measured in the number of frames, or the amount of time between keyframes '}")
        vop.gop_label.setToolTip(f"{'gop_label' if self.debug else 'Distance between two keyframes, measured in the number of frames, or the amount of time between keyframes'}")
        vop.keep_fps.setToolTip(f"{'keep_fps' if self.debug else 'Keep orignal framerate from the source video file '}")
        vop.fps.setToolTip(f"(frames per second) {'fps' if self.debug else 'Framerate to use for the encoded media file '}")
        vop.fps_label.setToolTip(f"{'fps_label' if self.debug else 'Framerate to use for the encoded media file'}")

        aop.a_mp3_bit_rate_label.setToolTip(f"{'a_mp3_bit_rate_label' if self.debug else 'BitRate to use with mp3 compression'}")
        aop.a_mp3_bit_rate.setToolTip(f"{'a_mp3_bit_rate' if self.debug else 'BitRate to use with mp3 compression'}")
        aop.use_og_audio.setToolTip(f"{'use_og_audio' if self.debug else 'Keep original soundtrack from the source media '}")
        aop.replace_audio.setToolTip(f"{'replace_audio' if self.debug else 'Replace the original audio track with another file '}")
        aop.audio_file_substitute.setToolTip(f"{'audio_file_substitute' if self.debug else 'Audio file to use as replacement in the encoded media '}")
        aop.audio_file_substitute_btn.setToolTip(f"{'audio_file_substitute_btn' if self.debug else 'Select an audio file to use as substitute '}")
        aop.sample_rate_label.setToolTip(f"{'sample_rate_label' if self.debug else 'Samplerate to use for the audio track of the media '}")
        aop.sample_rate.setToolTip(f"{'sample_rate' if self.debug else 'Samplerate to use for the audio track of the media '}")
        vop.video_group.setToolTip(f"{'video_group' if self.debug else 'Video Options'}")
        vop.hflip.setToolTip(f"{'hflip' if self.debug else 'Flip the video horizontally '}")
        vop.vflip.setToolTip(f"{'vflip' if self.debug else 'Flip the video vertically '}")
        vop.reverse.setToolTip(f"{'reverse' if self.debug else 'Reverse the video track '}")
        oop.use_prefix.setToolTip(f"{'use_prefix' if self.debug else 'Add a prefix at the begining of the destination filename '}")
        oop.out_prefix.setToolTip(f"{'out_prefix' if self.debug else 'Prefix to be used in the destination filename '}")
        oop.use_suffix.setToolTip(f"{'use_suffix' if self.debug else 'Add a suffix at the end of the destination filename '}")

        oop.out_suffix.setToolTip(f"{'out_suffix' if self.debug else 'Suffix to be used in the destination filename '}")
        oop.show_count.setToolTip(f"{'show_count' if self.debug else 'Append the number of frames in the destination filename '}")
        oop.dest.setToolTip(f"{'dest' if self.debug else 'Destination folder for the encoded media file '}")
        oop.dest_label.setToolTip(f"{'dest_label' if self.debug else 'Destination folder for the encoded media file '}")
        oop.dest_button.setToolTip(f"{'dest_button' if self.debug else 'Select a folder to save the converted media file '}")
        oop.overwrite.setToolTip(f"{'overwrite' if self.debug else 'Overwrite any existing file with the same name '}")

        aap.btn_folder.setToolTip(f"{'aap.btn_folder' if self.debug else 'Add a folder containing medias to convert'}")
        aap.btn_file.setToolTip(f"{'aap.btn_file' if self.debug else 'Add a media file to convert'}")
        aap.btn_del.setToolTip(f"{'aap.btn_del' if self.debug else 'Remove file from the list'}")
        aap.btn_clear.setToolTip(f"{'aap.btn_clear' if self.debug else 'Removes all files from the list'}")
        aap.btn_convert.setToolTip(f"{'aap.overwrite' if self.debug else 'Convert selected files'}")
        aap.listView.setToolTip(f"{'aap.listView' if self.debug else 'Drop your media files here !' }")
        aap.cmd_displayed.setToolTip(f"{'aap.command' if self.debug else 'FFmpeg command passed to the interpreter'}")
        aap.pbar.setToolTip(f"{'aap.pbar' if self.debug else 'Show the progress of the conversion (Experimental)'}")

        mop.tabs.setToolTip(f"{'mop.tabs' if self.debug else 'Options categories'}")
        mop.options_group.setToolTip(f"{'mop.options_group' if self.debug else 'Show options used for the conversion'}")
        aap.fast_encode.setToolTip(f"{'fast_encode' if self.debug else 'Presets list'}")
        aap.fast_label.setToolTip(f"{'fast_label' if self.debug else 'Select Preset'}")




