# Encoder e-medias
Fast video format converter using a Qt interface for ffmpeg, written by Cosmin Planchon. 
Please report bugs via [Issues ](../../issues) in the left sidebar. 
(Since I am using pyside2 this should be used with python 3.7.9)

## Installation
Requires ffmpeg:
download from https://ffmpeg.org/download.html
or alternatively get the windows binaries from : https://www.gyan.dev/ffmpeg/builds/ffmpeg-git-full.7z

on linux:
git clone https://gitlab.com/c.planchon/encoder_e-medias.git

python3 encoder_e-medias/__main__.py

on windows:
download python 3.7 from https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe
Note: during python installation make sure to have "Add Python 3.7 to PATH" enabled on the first page and check "install for all users" on the 3rd installation page.
type "cmd" in the Searchbar to open a Command Console.
in the cmd console : python "C:\ PATH_To \ encoder_e-medias"
(adjust to fit the encoder_e-medias folder location but keep the quotes " " marks)

After the first launch you need to set the Ffmpeg folder path from the top menu Edit > Ffmpeg path
(usually something like ffmpeg_folder/bin)

## Documentation
Drag & Drop your .mov or images sequence folder into the main window and hit convert.

Note: When converting image sequences to movies, the images filenames must have a sequential name pattern like img_001.png,img_002.png

Work in progress

