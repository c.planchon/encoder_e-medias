# -*- coding: utf-8 -*-
"""
Options for the media files conversion.
"""

from PyQt6.QtCore import Qt , QSize
from PyQt6.QtWidgets import QMainWindow, QWidget, QSizePolicy, QPushButton, QStyle, QComboBox, QFileDialog, QListView, QGridLayout, QLineEdit, QLabel, QTabWidget, QGroupBox, QVBoxLayout, QCheckBox, QHBoxLayout
from PyQt6.QtGui import QStandardItem, QIcon, QStandardItemModel, QFontDatabase, QFont, QPalette, QColor
from pathlib import Path
from utils import Utils
from options_video import OptionsVideo
from options_audio import OptionsAudio
from options_output import OptionsOutput
import ressources

class OptionsPanel(QMainWindow):
    """
    Secondary panel containing the options and parameters used during the conversion.
    """
    def __init__(self, application, parent=None):
        """
        Main method initialising the widget.

        Args:
            application: class instanciating this object.
            parent: parent class.
        """
        super(OptionsPanel, self).__init__(parent=parent)
        self.setWindowTitle('Options')
        self.parent = parent
        self.app = application
        self.ut = Utils()
        self._dir = self.app._dir
        self.substitute_audio_file = None
        self.destination_folder = None
        self.tmp_prefix = ""
        self.tmp_suffix = ""
        self.styled = open(f"{self._dir}/styles.css").read()
        self.video_widget = OptionsVideo(self,objectName="video_widget")
        self.audio_widget = OptionsAudio(self,objectName="audio_widget")
        self.output_widget = OptionsOutput(self,objectName="output_widget")
        self.vert_lt = QVBoxLayout()
        self.vert_lt.setObjectName("vert_lt")
        #self.setLayout(self.vert_lt)
        self.layout_out = QVBoxLayout()
        self.layout_out.setObjectName("lay_Out")
        self.vert_lt.setProperty("basegrey", "on")
        self.layout_out.setProperty("basegrey", "on")
        self.options_group = QGroupBox("Options")
        self.options_group.setCheckable(True)
        self.options_group.setProperty("subtab", "on")
        self.options_group.setProperty("arrowed", "on")
        self.vert_lt.addWidget(self.options_group)
        self.setCentralWidget(self.options_group)
        self.options_group.setLayout(self.layout_out)
        self.layout_out_line1 = QVBoxLayout()
        self.layout_out.addLayout(self.layout_out_line1)
        self.layout_options = QVBoxLayout()
        self.layout_options.setObjectName("layout_options")
        self.layout_options.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.tab_line = QVBoxLayout()
        self.tab_line.setObjectName("tab_line")
        self.layout_out_line1.addLayout(self.tab_line)
        self.tabs = QTabWidget()
        self.tab_line.addWidget(self.tabs, Qt.AlignmentFlag.AlignLeft)
        self.tabs.addTab(self.audio_widget,'Audio')
        self.tabs.addTab(self.video_widget,'Video')
        self.tabs.addTab(self.output_widget,'Output')
        self.set_defaults()
        self.audio_widget.audio_file_substitute_btn.clicked.connect(self.add_a_sound_file)
        self.output_widget.dest_button.clicked.connect(self.set_target_dir)
        self.output_widget.use_prefix.stateChanged.connect(self.set_prefix)
        self.output_widget.vid_from_img.stateChanged.connect(self.cb_vid_from_img)
        self.output_widget.use_suffix.stateChanged.connect(self.set_suffix)
        self.output_widget.keep_origin_folder.stateChanged.connect(self.set_origin_dest)
        self.video_widget.use_og_video.stateChanged.connect(self.set_og_vstream)
        self.audio_widget.use_og_audio.stateChanged.connect(self.set_og_astream)
        self.video_widget.keep_fps.stateChanged.connect(self.set_og_fps)
        self.video_widget.enum_video_codecs.currentTextChanged.connect(self.adjust_extension)
        self.options_group.toggled.connect(self.show_options)
        self.output_widget.enum_out_ext.currentTextChanged.connect(self.enum_out_ext_cb)
        self.video_widget.watermark_file_btn.clicked.connect(self.app.app.set_watermark_url)

    def show_options(self):
        self.tabs.setVisible(self.options_group.isChecked())
        self.setStyleSheet(self.styled)

    def cb_vid_from_img(self):
        self.app.mc.multi_images_mode = False
        if self.output_widget.vid_from_img.isChecked():
            if len(self.app.list_of_files)>1:
                self.multi_images_mode = True
            self.video_widget.use_scaler.setChecked(True)
            self.video_widget.compat_mode.setChecked(True)
            self.output_widget.still_vid_lbl.show()
            self.output_widget.still_vid_duration.show()
            self.output_widget.frame_length_lbl.show()
            self.output_widget.frame_length.show()
        else:
            self.output_widget.still_vid_lbl.hide()
            self.output_widget.still_vid_duration.hide()
            self.output_widget.frame_length_lbl.hide()
            self.output_widget.frame_length.hide()

    def set_tab(self,index):
        size = None
        if index == 0 :
            size = self.audio_widget.audio_group.size()
            size_x, size_y = size.width(),size.height()
            self.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
            self.resize(650, 750)

        if index == 1 :
            size = self.video_widget.video_group.size()
            size_x, size_y = size.width(),size.height()
            self.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
            self.resize(650, 750)

        if index == 2 :
            size = self.output_widget.out_group.size()
            size_x, size_y = size.width(),size.height()
            self.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
            self.resize(650, 750)

    def display_codec_options(self):
        codec = self.video_widget.enum_video_codecs.currentText()
        self.video_widget.qual_group.setVisible(codec == "libx264")
    
    def enum_out_ext_cb(self,cb=None):
        ext = self.output_widget.enum_out_ext.currentText()
        img_formats = self.ut.get_img_formats()
        if ext in img_formats:
            self.app.mc.output_img_sequence = True
        else:
            self.app.mc.output_img_sequence = False

    def adjust_extension(self):
        self.display_codec_options()
        cod = self.video_widget.enum_video_codecs.currentText()
        if "prores" in cod:
            self.output_widget.enum_out_ext.setCurrentText(".mov")
        elif "libtheora" in cod:
            self.output_widget.enum_out_ext.setCurrentText(".ogv")
        elif "gif" in cod:
            self.output_widget.enum_out_ext.setCurrentText(".gif")
        elif "libwebp" in cod:
            self.output_widget.enum_out_ext.setCurrentText(".webp")
        elif "cinepak" in cod:
            self.output_widget.enum_out_ext.setCurrentText(".mov")

    def set_og_fps(self):
        state = self.video_widget.keep_fps.checkState() == Qt.CheckState.Checked
        self.video_widget.fps.setDisabled(state)
        self.video_widget.fps_label.setDisabled(state)

    def set_og_vstream(self):
        vw = self.video_widget
        state = vw.use_og_video.checkState() == Qt.CheckState.Checked or not vw.video_group.isChecked()

        attr_list = [vw.fps, vw.fps_label, vw.vflip, vw.hflip,vw.use_scaler, vw.scaler,
                    vw.compat_mode, vw.use_preset, vw.enum_preset,vw.use_tune, vw.enum_tunes,
                    vw.use_profiles, vw.enum_profile, vw.codec_label, vw.enum_video_codecs,
                    vw.qual_group, vw.reverse, vw.keep_fps]
        for attr in attr_list:
            attr.setDisabled(state)

            if state:

                attr.setToolTip("Disabled when keeping original video stream")
            else:
                self.set_og_fps()
                self.app.easy.set_tooltips()

    def set_og_astream(self):
        aw = self.audio_widget
        attr_list = [aw.audio_codec,aw.audio_file_substitute_btn, aw.replace_audio, aw.reverse_audio, aw.audio_file_substitute,aw.enum_audio_codecs, aw.sample_rate, aw.a_mp3_bit_rate_label, aw.a_mp3_bit_rate, aw.sample_rate_label]
        state = aw.use_og_audio.checkState() == Qt.CheckState.Checked
        for attr in attr_list:
            attr.setDisabled(state)
            if state:
                attr.setToolTip("Disabled when keeping original audio stream")
            else:
                self.app.easy.set_tooltips()

    def set_suffix(self):
        state = not self.output_widget.use_suffix.isChecked()
        self.output_widget.out_suffix.setDisabled(state)
        if state :
            self.tmp_suffix = self.output_widget.out_suffix.text()
            self.output_widget.out_suffix.setText("")
        else:
            if self.output_widget.out_suffix.text() == "" :
                self.output_widget.out_suffix.setText(self.tmp_suffix)

    def set_prefix(self):
        state = not self.output_widget.use_prefix.isChecked()
        self.output_widget.out_prefix.setDisabled(state)
        if state :
            self.tmp_prefix = self.output_widget.out_prefix.text()
            self.output_widget.out_prefix.setText("")
        elif (self.output_widget.out_suffix.text() == "" ):
                self.output_widget.out_prefix.setText(self.tmp_prefix)

    def set_origin_dest(self):
        state = not self.output_widget.keep_origin_folder.isChecked()
        self.output_widget.dest.setEnabled(state)
        self.output_widget.dest_label.setEnabled(state)
        self.output_widget.dest_button.setEnabled(state)

    def add_a_sound_file(self):
        """
        Select a sound file to use as audio track.
        """
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode.ExistingFiles)
        dialog.setOption(QFileDialog.Option.ShowDirsOnly,False)
        dialog.exec()
        self.substitute_audio_file = dialog.selectedFiles()
        self.audio_widget.audio_file_substitute.setText(self.substitute_audio_file[0])

    def set_target_dir(self):
        self.destination_folder = self.set_selected_dir()
        self.output_widget.dest.setText(self.destination_folder)
    
    
    def set_selected_dir(self):
        """
        Sets the target ouput directory.
        """
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode.Directory)
        dialog.setOption(QFileDialog.Option.DontUseNativeDialog,True)
        dialog.setOption(QFileDialog.Option.ShowDirsOnly,False)
        dialog.exec()
        return dialog.directory().path()

    def set_defaults(self):
        self.audio_widget.audio_group.setChecked(True)
        self.video_widget.video_group.setChecked(True)
        self.audio_widget.use_og_audio.setChecked(True)
        self.output_widget.use_suffix.setChecked(True)
        self.output_widget.overwrite.setChecked(True)
        self.output_widget.keep_origin_folder.setChecked(True)
        self.video_widget.keep_fps.setChecked(True)
