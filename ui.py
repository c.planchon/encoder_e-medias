# -*- coding: utf-8 -*-
"""
Main User interface of the app.
"""
from PyQt6.QtCore import Qt, QModelIndex, QItemSelectionModel,QSize
from PyQt6.QtWidgets import QWidget, QFrame, QPushButton, QStyle, QApplication, QComboBox, QGraphicsDropShadowEffect, QListView, QGridLayout, QMenu, QLineEdit, QLabel, QGroupBox, QVBoxLayout, QHBoxLayout, QCheckBox, QFileDialog, QProgressBar
from PyQt6.QtGui import QAction,QCursor, QStandardItem, QIcon, QStandardItemModel, QFontDatabase, QFont, QPalette, QColor
from pathlib import Path
from converter import MovieConverter
from utils import Utils
from options import OptionsPanel
from easydev import EasyDev
import ressources
import json

class PopupFrame(QFrame):
    """Status Setter frame.

        >>> Popup frame used to display the Status setter options.

        Args:
            None

        Returns:
            None.
    """
    def __init__(self, _app=None):
        super().__init__()
        self.dragging = False
        self.center_wdgt = QWidget(self)
        self.center_wdgt.setProperty("popup","on")
        #self.setProperty("popup","on")
        self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)
        

    def resizeEvent(self, event):
        self.center_wdgt.resize(QSize(self.width(),self.height()))
        super().resizeEvent(event)

    def mousePressEvent(self, event):
        """
        Capture the mouse press event to initiate dragging.
        """
        if event.button() == Qt.LeftButton:
            # Store the position where the mouse was clicked
            self.dragging = True
            self.drag_start_pos = event.globalPosition() - self.frameGeometry().topLeft()
            event.accept()

    def mouseMoveEvent(self, event):
        """
        Handle mouse movement to drag the frame.
        """
        if self.dragging:
            # Move the window to follow the mouse, while maintaining the offset
            self.move(event.globalPosition() - self.drag_start_pos)
            event.accept()

    def mouseReleaseEvent(self, event):
        """
        Stop dragging when the mouse button is released.
        """
        if event.button() == Qt.LeftButton:
            self.dragging = False
            event.accept()

class MainPanel(QWidget):
    """
    Main widget displaying the list of files to convert.
    """

    def __init__(self, application, parent=None):
        """
        Main method initializing the widget.

        Args:
            application: class instanciating this object.
            parent: parent class.
        """
        super(MainPanel, self).__init__(parent=parent)
        #self.setGeometry(300, 300, 290, 150)
        self.setWindowTitle('Input dialog')
        self.setAcceptDrops(True)
        self.selected_lines = None
        self.list_of_files = []
        self.bin_path = None
        
        self._dir = str(Path(__file__).parent)
        self.main_grid = QVBoxLayout()
        self.group_layout = QVBoxLayout()
        self.main_group = QGroupBox()
        self.main_group.setProperty("mainbase", "on")
        self.setLayout(self.main_grid)
        self.main_group.setLayout(self.group_layout)
        self.main_grid.addWidget(self.main_group)
        self.app = application
        self.layout_grid = QGridLayout()
        self.group_layout.addLayout(self.layout_grid)
        self.all_items = []
        self.parent = parent
        self.listView = QListView(self)
        self.items = None
        self.ut = Utils()
        self.mc = MovieConverter(self)
        self.logger = self.mc.logger
        self.setWindowTitle('Encoder e-medias')
        self.btn_folder = QPushButton("Add folder")
        self.btn_file = QPushButton("Add file")
        self.btn_del = QPushButton("Remove file")
        self.btn_clear = QPushButton("Clear list")
        self.btn_convert = QPushButton("Convert")
        
        self.placeholder1 = QLabel(" ")
        self.placeholder2 = QLabel(" ")

        self.layout_grid.addWidget(self.btn_folder, 0, 0)
        self.layout_grid.addWidget(self.btn_file, 0, 1)
        self.layout_grid.addWidget(self.btn_del, 0, 2)
        self.layout_grid.addWidget(self.btn_clear, 0, 3)
        self.layout_grid.addWidget(self.listView, 1, 0,1,4)
        self.drop_here = QLabel("Drop Media Files Here")
        self.drop_here.setProperty("transluent","on")
        self.layout_grid.addWidget(self.drop_here, 1, 0,1,4,Qt.AlignmentFlag.AlignCenter)
        self.layout_grid.addWidget(self.btn_convert, 2, 0)
        self.model = QStandardItemModel(self.listView)
        self.listView.setModel(self.model)
        self.listView.selectionModel().selectionChanged.connect(self.selection_changed)
        self.fast_label = QLabel("Fast encode to:")
        self.fast_encode = QComboBox()
        self.add_preset_btn = QPushButton()
        self.del_preset_btn = QPushButton()
        self.dialog_delete_label = QLabel("Confirm Delete ?")
        self.confirm_delete_btn = QPushButton("Delete")
        self.cancel_delete_btn = QPushButton("Cancel")
        minus_icon = QIcon(':icon_minus.png')
        plus_icon = QIcon(':icon_plus.png')
        self.del_preset_btn.setIcon(minus_icon)
        self.del_preset_btn.setIconSize(QSize(12,12))
        #self.del_preset_btn.setStyleSheet("background-image: url('img/icon_checkbox_minus.png'); margin:0px; padding:0px;")
        #self.add_preset_btn.setStyleSheet("background-image: url('img/icon_checkbox_plus.png'); margin:0px; padding:0px;")
        self.add_preset_btn.setIcon(plus_icon)
        self.add_preset_btn.setFlat(True)
        self.del_preset_btn.setFlat(True)
        self.add_preset_btn.setIconSize(QSize(12,12))
        self.add_preset_btn.setFixedHeight(12)
        self.add_preset_btn.setFixedWidth(12)
        self.del_preset_btn.setFixedHeight(12)
        self.del_preset_btn.setFixedWidth(12)
        self.fast_label.setFixedWidth(60)
        self.fast_encode.setFixedWidth(120)
        buttons_layout = QVBoxLayout()
        preset_line_layout = QHBoxLayout()
        preset_line_layout.addWidget(self.fast_label, Qt.AlignmentFlag.AlignRight)
        preset_line_layout.addWidget(self.fast_encode,Qt.AlignmentFlag.AlignRight)
        preset_line_layout.addLayout(buttons_layout, Qt.AlignmentFlag.AlignRight)
        self.save_preset_btn = QPushButton("Save Preset")
        self.cancel_preset_btn = QPushButton("Cancel")
        buttons_layout.addWidget(self.add_preset_btn, Qt.AlignmentFlag.AlignLeft)
        buttons_layout.addWidget(self.del_preset_btn, Qt.AlignmentFlag.AlignLeft)
        self.fast_encode.addItems(self.ut.get_fast_options())

        self.layout_grid.addLayout(preset_line_layout, 2, 3, )
        self.set_items()
        self.btn_folder.clicked.connect(self.showDialog)
        self.btn_file.clicked.connect(self.add_a_file)
        self.btn_del.clicked.connect(self.remove_file)
        self.btn_clear.clicked.connect(self.clear_list)
        self.btn_convert.clicked.connect(self.activated)
        self.fast_encode.activated.connect(self.do_fast_setup)
        self.pbar = QProgressBar()
        self.cmd_displayed = QLineEdit("ffmpeg ...")
        self.cmd_displayed.setProperty("command","on")
        self.op = OptionsPanel(self)
        self.easy = EasyDev(self)
        self.op.options_group.setChecked(False)
        self.layout_grid.addWidget(self.op, 3,0,5,4)
        self.main_grid.addWidget(self.pbar)
        self.op.set_og_astream()
        self.op.set_origin_dest()
        self.op.set_prefix()
        self.op.set_og_fps()
        self.main_grid.addWidget(self.cmd_displayed)
        self.easy.set_tooltips()
        self.pbar.hide()
        self.main_grid.setContentsMargins(0,0,0,0)
        self.main_grid.setSpacing(0)
        self.new_preset_name_field = QLineEdit()
        self.new_preset_name_label = QLabel("New Preset Name:")
        self.preset_adder_frame = PopupFrame()
        self.preset_remover_frame = PopupFrame()
        self.preset_adder_frame.setWindowFlags(Qt.WindowType.FramelessWindowHint)
        self.preset_remover_frame.setWindowFlags(Qt.WindowType.FramelessWindowHint)

        preset_remover_layout_0 = QVBoxLayout(self.preset_remover_frame)
        preset_remover_layout_0.setProperty("popup","on")
        preset_remover_layout_1 = QHBoxLayout()
        preset_remover_layout_0.addWidget(self.dialog_delete_label)
        preset_remover_layout_1.addWidget(self.cancel_delete_btn)
        preset_remover_layout_1.addWidget(self.confirm_delete_btn)
        preset_remover_layout_0.addLayout(preset_remover_layout_1)

        preset_adder_layout_0 = QVBoxLayout(self.preset_adder_frame)
        preset_adder_layout_0.setProperty("popup","on")
        preset_adder_layout_1 = QHBoxLayout()
        preset_adder_layout_1.addWidget(self.new_preset_name_label)
        preset_adder_layout_1.addWidget(self.new_preset_name_field)
        self.add_preset_btn.clicked.connect(self.show_add_preset_name)
        self.del_preset_btn.clicked.connect(self.show_delete_popup)
        self.cancel_preset_btn.clicked.connect(lambda:self.preset_adder_frame.hide())
        self.cancel_delete_btn.clicked.connect(lambda:self.preset_remover_frame.hide())
        self.save_preset_btn.clicked.connect(lambda:self.app.save_preset(self.new_preset_name_field.text()))
        self.confirm_delete_btn.clicked.connect(self.delete_preset)
        preset_adder_layout_2 = QHBoxLayout()
        preset_adder_layout_0.addLayout(preset_adder_layout_1)
        preset_adder_layout_0.addLayout(preset_adder_layout_2)
        preset_adder_layout_2.addWidget(self.cancel_preset_btn)
        preset_adder_layout_2.addWidget(self.save_preset_btn)
        attr_list = [self.btn_convert,self.pbar,self.del_preset_btn,self.save_preset_btn,self.cancel_preset_btn,self.add_preset_btn,self.btn_del,self.btn_folder,self.btn_file,self.btn_clear]
        for attr in attr_list:
            effect = QGraphicsDropShadowEffect(self)
            effect.setBlurRadius(1)
            effect.setOffset(0, 2)
            effect.setColor(QColor(16, 16, 16, 48))
            attr.setGraphicsEffect(effect)

    def delete_preset(self):
        Path(__file__).parent.joinpath('presets').joinpath(self.fast_encode.currentText()+'.json').unlink()
        self.fast_encode.model().clear()
        self.fast_encode.addItems(self.ut.get_fast_options())
        self.preset_remover_frame.hide()

    def show_delete_popup(self):
        self.dialog_delete_label.setText(f"Do you really want to delete \n{self.fast_encode.currentText()} ?")
        mouse_position = QCursor.pos()
        self.preset_remover_frame.move(mouse_position.x()-100, mouse_position.y() - 20)
        self.preset_remover_frame.show()

    def show_add_preset_name(self):
        mouse_position = QCursor.pos()
        self.preset_adder_frame.move(mouse_position.x()-100, mouse_position.y() - 20)
        self.preset_adder_frame.show()

    def fade_helper_label(self):
        if self.model.rowCount() > 0 :
            self.drop_here.setStyleSheet("""QLabel { color: rgba(200,200,200,0); }""")
        else:
            self.drop_here.setStyleSheet("""QLabel {
                    font-size: 44px;
                    font-weight: bold;
                    color: rgba(200,200,200,20); }""")

    def do_fast_setup(self):
        """
        Sets various parameters for audio/video encoding when using the 'Fast encode' dropdown.
        """
        self.app.load_preset()
        """
        sel = self.fast_encode.currentText()
        vid = self.op.video_widget
        aud = self.op.audio_widget
        out = self.op.output_widget
        if sel == "None" :
            return
        elif sel == "X264 LOW" :
            vid.crf.setText("40")
            vid.use_crf.setChecked(True)
            vid.gop.setText("40")
            vid.enum_video_codecs.setCurrentText("libx264")
            vid.use_og_video.setChecked(False)
            vid.video_group.setChecked(True)
            return
        elif sel == "X264 MID" :
            vid.crf.setText("20")
            vid.use_crf.setChecked(True)
            vid.gop.setText("30")
            vid.enum_video_codecs.setCurrentText("libx264")
            vid.use_og_video.setChecked(False)
            vid.video_group.setChecked(True)
            return
        elif sel == "X264 HIGH" :
            vid.crf.setText("15")
            vid.use_crf.setChecked(True)
            vid.gop.setText("20")
            vid.enum_video_codecs.setCurrentText("libx264")
            vid.use_og_video.setChecked(False)
            vid.video_group.setChecked(True)
            return
        elif sel == "X264 LOSSLESS" :
            vid.crf.setText("5")
            vid.use_crf.setChecked(True)
            vid.gop.setText("10")
            vid.enum_video_codecs.setCurrentText("libx264")
            vid.use_og_video.setChecked(False)
            vid.video_group.setChecked(True)
            return
        elif sel == "Media to Wav" :
            aud.audio_group.setChecked(True)
            vid.video_group.setChecked(False)
            aud.use_og_audio.setChecked(False)
            aud.enum_audio_codecs.setCurrentText("pcm_s16le")
            out.enum_out_ext.setCurrentText(".wav")
            return
        elif sel == "Media to Mp3" :
            aud.audio_group.setChecked(True)
            vid.video_group.setChecked(False)
            aud.use_og_audio.setChecked(False)
            aud.enum_audio_codecs.setCurrentText("libmp3lame")
            out.enum_out_ext.setCurrentText(".mp3")
            return
        elif sel == "Slideshow from images" :
            #aud.audio_group.setChecked(True)
            vid.video_group.setChecked(True)
            #aud.use_og_audio.setChecked(False)
            #aud.enum_audio_codecs.setCurrentText("libmp3lame")
            out.enum_out_ext.setCurrentText(".mp4")
            out.vid_from_img.setChecked(True)
            vid.keep_fps.setChecked(False)
            return
        else :
            print("not implemented yet, please set it manually")
            return
        """
    def call_sel_change(self,index):
        #self.logger.error("changing selection")
        self.mc.set_cmd_dict()
        if len(self.listView.selectionModel().selectedRows(0)) > 0 and self.listView.model().rowCount() > 0 :
            self.mc.mov = (index.indexes()[0].data())
            self.mc.refresh_cmd_args()
            self.mc.prep_run()
        else:
            self.mc.reset_ffmpeg_bin_path()
            self.mc.show_command()

    def selection_changed(self,index):
        self.call_sel_change(index)

    def show_options(self):
        """
        Calls the method displaying the options window.
        """
        self.op.show()

    def dragEnterEvent(self, event):
        """
        Triggered when initiating a drag'n drop event.

        Args:
            event: Qt event.
        """
        for obj in event.mimeData().urls():
            if not Path(obj.toLocalFile()).is_file():
                event.ignore()
        if event.mimeData().hasImage:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        """
        Triggered when dragging with the mouse on the window.

        Args:
            event: Qt event.
        """
        for obj in event.mimeData().urls():
            if not Path(obj.toLocalFile()).is_file():
                event.ignore()
        if event.mimeData().hasImage:
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        Triggered when dropping an element in the window.

        Args:
            event: Qt event.
        """
        if event.mimeData().hasImage:
            #self.logger.error("dropped files")
            event.setDropAction(Qt.DropAction.CopyAction)
            _files = [file.toLocalFile() for file in event.mimeData().urls()]
            self.add_files(_files)

    def remove_file(self):
        """
        Removes the selected line from the list of files to convert.
        """
        selected = self.listView.currentIndex().data(Qt.DisplayRole)
        if selected != None:
            self.items.remove(selected)
            self.list_of_files = self.items
            self.listView.model().removeRow(self.listView.selectionModel().currentIndex().row())
            self.listView.update()
            self.set_items()

    def clear_list(self):
        """
        Clears the list of files to convert.
        """
        self.items = []
        self.list_of_files = []
        self.all_items = []
        self.listView.model().clear()
        self.set_items()

    def remove_duplicates(self, items):
        seen = set()
        for item in items:
            key = (item.text(), item.data())
            if key not in seen:
                seen.add(key)
                self.model.appendRow(item)

    def set_items(self):
        """
        Populates the list of items displayed in the listView.
        """
        #self.logger.error("setting items list")
        self.all_items.clear()
        self.listView.model().clear()
        self.mc._ffmpeg_args.clear()
        self.mc.command.clear()
        self.mc.set_cmd_dict()
        if self.items != None :
            #self.logger.error("adding path in self.items")
            for path in self.items:
                standardItem = QStandardItem(path)
                standardItem.setCheckable(True)
                standardItem.setCheckState(Qt.CheckState.Checked)
                self.all_items.append(standardItem)
            self.remove_duplicates(self.all_items)
            #self.logger.error("removed duplicates from self.all_items and appending to model")
            self.listView.setModel(self.model)
            index = self.listView.model().index(0,0)
            self.listView.selectionModel().select(index, QItemSelectionModel.SelectionFlag.Select)

        #self.logger.error("model set")
        self.fade_helper_label()

    def uncheck_all(self):
        for itm in self.all_items:
            itm.setCheckState(Qt.CheckState.Unchecked)

    def activated(self):
        """
        Calls the conversion method with the current parameters.
        """
        self.selected_lines = self.items_selected()
        self.mc.convert(self.selected_lines)

    def untick_checkbox(self):
        model = self.listView.model()
        i = 0
        while model.item(i):
            if model.item(i).checkState() == Qt.CheckState.Checked :
                selected.append(model.item(i).text())
            i += 1
        return selected

    def set_ffmpeg_path(self):
        selected = self.op.set_selected_dir()
        if Path(selected).is_dir() :
            self.bin_path = selected
            self.mc.reset_ffmpeg_bin_path()
        self.app.save_config()

    def items_selected(self):
        """
        Provides the item currently selected in the listView.
        """
        selected = []
        model = self.listView.model()
        i = 0
        while model.item(i):
            if model.item(i).checkState() == Qt.CheckState.Checked :
                selected.append(model.item(i).text())
            i += 1
        return selected

    def is_image_in_sequence(self,image_path):
        """
        Check if an image is part of a sequence in the given folder.

        Args:
            image_path (str): Path to the image file.
            
        Returns:
            bool: True if the image is part of a sequence, False otherwise.
        """
        # Create a sequence from the folder's contents
        #sequences = self.ut.all_seqs(f"{Path(image_path).parent}")
        sequences = self.ut.get_seqs(f"{Path(image_path).parent}")
        
        # Get the basename of the image file (filename without path)
        image_name = f"{Path(image_path).name}"
        #self.logger.error(f"searching {image_name} in {sequences}")
        # Check each sequence for the image
        for seq in sequences:
            #self.logger.error([str(sq) for sq in list(seq)])
            if image_name in [str(sq) for sq in list(seq)]:
                return True
        return False

    def add_dir_seq(self,_file,lst):
        for seq in self.ut.get_seqs(_file):
            if (seq.parts)[0] == '':
                #no prefix fix
                sig = len((seq.digits)[0])
                lst.append(f"{_file}/%{sig:02d}d{(seq.parts)[1]}")
                self.logger.error(f" appending {_dir}/%{sig:02d}d{(seq.parts)[1]}")
            else:
                self.logger.error(f"appending {_dir}/{seq.format('%h%p%t')}")
                lst.append(f"{_file}/{seq.format('%h%p%t')}")
        return lst

    def add_img_seq(self,_file,lst):
        _dir = f"{Path(_file).parent}"
        for seq in self.ut.get_seqs(_dir):
            if f"{Path(_file).name}" in [str(sq) for sq in list(seq)]:
                if (seq.parts)[0] == '':
                    #no prefix fix
                    sig = len((seq.digits)[0])
                    lst.append(f"{_dir}/%{sig:02d}d{(seq.parts)[1]}")
                    self.logger.error(f" appending {_dir}/%{sig:02d}d{(seq.parts)[1]}")
                else:
                    self.logger.error(f"appending {_dir}/{seq.format('%h%p%t')}")
                    lst.append(f"{_dir}/{seq.format('%h%p%t')}")
        return lst

    def add_files(self,files):
        """
        Adds a bunch of files to the conversion queue.

        Args:
            files: list of file path (str), cannot be None.
        """
        if self.list_of_files is None:
            self.list_of_files = []
        files_dropped = []
        for _file in files:
            #if file is folder
            if Path(str(_file)).is_dir():
                #self.logger.error("got DIR")
                movs = list(self.scan_for_ext(_file, 'mov'))
                if len(movs) > 0:
                    files_dropped += movs
                elif(self.has_image_seq(_file)):
                    print("seq detected")
                    files_dropped = self.add_dir_seq(_file,files_dropped)
                else:
                    print(f"skipping{_file}")
                    continue
            #if file is file
            elif Path(str(_file)).is_file():
                self.logger.error("got FILE")
                if Path(str(_file)).suffix == ".pls":
                    with open(_file, 'r') as playlist:
                        self.list_of_files = ((list(json.load(playlist).values())))
                    continue
                if not self.is_image_in_sequence(_file):
                    self.logger.error("just FILE")
                    files_dropped.append(_file)
                else:
                    files_dropped = self.add_img_seq(_file,files_dropped)
                    self.logger.error(f"got SEQ ! {files_dropped}")
            #if file is img_seq
            elif "%" in _file :
                self.logger.error("got image sequence")
                files_dropped.append(_file)

        if files_dropped != []:
            if isinstance(self.list_of_files, set):
                self.list_of_files = list(self.list_of_files)
            for _file in files_dropped:
                #self.logger.error(f"appending {_file}")
                self.list_of_files.append(_file)

        if self.list_of_files not in ([], None):
            #print(f"list_of_files is {self.list_of_files}")
            self.items = set(self.list_of_files)
            self.set_items()

    def has_image_seq(self,folder):
        """
        Tests for valid image sequences in the folder.

        Args:
            folder: folder path (str), cannot be None.

        Returns:
            True if a sequence of more than 1 image is detected by pyseq, False otherwise.
        """
        return len(self.ut.get_seqs(folder)) > 0

    def add_a_file(self):
        """
        Adds a single file to the conversion list.
        """
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode.ExistingFiles)
        dialog.setOption(QFileDialog.Option.ShowDirsOnly,False)
        dialog.exec()
        if self.list_of_files is None:
            self.list_of_files = []
        self.add_files(dialog.selectedFiles())

    def showDialog(self):
        """
        Opens a file browser to select files or folders containing media files.
        """
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode.Directory)
        dialog.setOption(QFileDialog.Option.DontUseNativeDialog,True)
        dialog.setOption(QFileDialog.Option.ShowDirsOnly,False)
        dialog.exec()
        _dir = dialog.directory().path()
        self.add_files(self.scan_for_ext(_dir,"mov"))

    def scan_for_ext(self,dir_path,ext):
        """
        Tests if the selected path contains a file with this extension.

        Args:
            dir_path: folder path (str), cannot be None.
            ext: file extension, including the dot '.', ex: .mov ), cannot be None.

        Returns:
            List of files from this folder using that extension [str, str...].
        """
        target = Path(f"{str(dir_path)}")
        print(f"scanning for {ext} in {target}")
        return [str(string) for string in target.glob(f"*.{ext}")]
