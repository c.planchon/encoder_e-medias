# -*- coding: utf-8 -*-
"""
Autocoder is a GUI for ffmpeg allowing you to queue and manipulate media files of various formats.

Supports drag'n drop via Qt and can handle image sequences thanks to pyseq.

__author__ = "Cosmin Planchon"
__license__ = "GPL"
__version__ = "1.1.0"
"""
import sys
from setup import DependencyChecker
try:
    from PyQt6 import QtWidgets
    from PyQt6 import QtCore
except ModuleNotFoundError:
    DependencyChecker().check_and_install_modules()
    from PyQt6 import QtWidgets
    from PyQt6 import QtCore

from ui import MainPanel
from main_window import MainWindow

class Autocoder:
    """
    Main class, runs the app and displays its window
    """

    def __init__(self, args):
        """
        Method used to initialize the class.

        Args:
            args: command line arguments passed when instanciating Autocoder.
        """
        #QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
        QtCore.QCoreApplication.setAttribute(QtCore.Qt.ApplicationAttribute.AA_ShareOpenGLContexts)
        self.pyside_app = QtWidgets.QApplication(sys.argv)
        self.main_window = MainWindow(application=self)
        self.main_window.panel.easy.debug = ('--debug' in args)
        self.main_window.panel.easy.set_tooltips()
        self.main_window.get_config()

    def run(self):
        self.main_window.show()
        self.main_window.setStyleSheet(self.main_window.panel.op.styled)
        self.main_window.panel.preset_adder_frame.setStyleSheet(self.main_window.panel.op.styled)
        self.main_window.panel.preset_remover_frame.setStyleSheet(self.main_window.panel.op.styled)
        return (self.pyside_app.exec())

if __name__ == '__main__':
    main_app = Autocoder(sys.argv)
    sys.exit(main_app.run())
