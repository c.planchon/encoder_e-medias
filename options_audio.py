# -*- coding: utf-8 -*-
from PyQt6.QtCore import Qt , QSize
from PyQt6.QtWidgets import QMainWindow,QWidget, QPushButton, QStyle, QGraphicsDropShadowEffect, QComboBox, QFileDialog, QListView, QGridLayout, QLineEdit, QLabel, QTabWidget, QGroupBox, QVBoxLayout, QCheckBox, QHBoxLayout
from PyQt6.QtGui import QStandardItem, QIcon, QStandardItemModel, QColor
from pathlib import Path
from utils import Utils

class OptionsAudio(QMainWindow):
    def __init__(self, application, parent=None,objectName=None):
        super(OptionsAudio, self).__init__(parent=parent)
        self.setObjectName(objectName)
        self.ut = Utils()
        self.enum_audio_codecs = QComboBox(objectName="enum_audio_codecs")
        self.enum_audio_codecs.addItems(self.ut.get_audio_codecs())
        self.app = application
        self.layout_audio = QVBoxLayout()
        self.layout_audio.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.audio_group = QGroupBox("Use Audio")
        self.audio_group.setProperty("subtab", "on")
        self.setCentralWidget(self.audio_group)
        self.audio_group.setAlignment(Qt.AlignmentFlag.AlignTop)
        self.audio_group.setFixedHeight(200)
        self.audio_group.setCheckable(True)
        self.audio_group.setLayout(self.layout_audio)
        self.layout_audio_line0 = QHBoxLayout()
        self.use_og_audio = QCheckBox("Keep original audio codec",objectName="use_og_audio")
        self.layout_audio_line0.addWidget(self.use_og_audio )
        self.layout_audio.addLayout(self.layout_audio_line0)
        self.layout_audio_line3 = QHBoxLayout()
        self.layout_audio.addLayout(self.layout_audio_line3)
        self.replace_audio = QCheckBox("Replace audio track:",objectName="replace_audio")
        self.audio_file_substitute = QLineEdit(objectName="audio_file_substitute")
        self.audio_file_substitute_btn = QPushButton()
        appIcon = QIcon(":/folder.png")
        self.audio_file_substitute_btn.setIcon(appIcon)
        self.placeholder10 = QLabel(" ")
        self.a_mp3_bit_rate_label = QLabel("Bitrate")
        self.a_mp3_bit_rate_label.setFixedWidth(100)
        self.a_mp3_bit_rate = QLineEdit("320",objectName="a_mp3_bit_rate")
        self.a_mp3_bit_rate.setFixedWidth(68)
        self.replace_audio.setFixedWidth(150)
        self.audio_file_substitute.setFixedWidth(250)
        self.audio_file_substitute_btn.setFixedWidth(26)
        self.layout_audio_line3.addWidget(self.replace_audio, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line3.addWidget(self.audio_file_substitute, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line3.addWidget(self.audio_file_substitute_btn, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line3.addWidget(self.placeholder10, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line4 = QHBoxLayout()
        self.layout_audio.addLayout(self.layout_audio_line4)
        self.audio_codec = QLabel("Audio Codec:")
        self.audio_codec.setFixedWidth(110)
        self.layout_audio_line4.addWidget(self.audio_codec, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line4.addWidget(self.enum_audio_codecs, Qt.AlignmentFlag.AlignLeft)
        self.placeholder3 = QLabel(" ")
        self.layout_audio_line4.addWidget(self.placeholder3, Qt.AlignmentFlag.AlignLeft)

        self.layout_audio_line5 = QHBoxLayout()
        self.layout_audio.addLayout(self.layout_audio_line5)
        self.layout_audio_line6 = QHBoxLayout()
        self.layout_audio.addLayout(self.layout_audio_line6)

        self.sample_rate_label = QLabel("Sample Rate:")
        self.sample_rate_label.setFixedWidth(100)
        self.sample_rate = QLineEdit("48000",objectName="sample_rate")
        self.sample_rate.setFixedWidth(68)
        self.placeholder4 = QLabel(" ")
        self.placeholder5 = QLabel(" ")

        self.layout_audio_line5.addWidget(self.sample_rate_label, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line5.addWidget(self.sample_rate, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line5.addWidget(self.placeholder4, Qt.AlignmentFlag.AlignLeft)

        self.layout_audio_line6.addWidget(self.a_mp3_bit_rate_label, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line6.addWidget(self.a_mp3_bit_rate, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line6.addWidget(self.placeholder5, Qt.AlignmentFlag.AlignLeft)
        self.layout_audio_line7 = QHBoxLayout()
        self.layout_audio.addLayout(self.layout_audio_line7)
        self.reverse_audio = QCheckBox("Reverse Audio",objectName="reverse_audio")
        self.layout_audio_line7.addWidget(self.reverse_audio)


        attr_list = [self.sample_rate, self.reverse_audio, self.sample_rate_label, self.audio_codec,self.audio_file_substitute_btn, self.audio_file_substitute, self.replace_audio, self.enum_audio_codecs, self.use_og_audio]
        self.set_shadows(attr_list)
        self.use_og_audio.setChecked(True)

    def set_shadows(self, attr_list):

        for attr in attr_list:
            effect = QGraphicsDropShadowEffect(self)
            effect.setBlurRadius(2)
            effect.setOffset(1, 2)
            effect.setColor(QColor(16, 16, 16, 64))
            attr.setGraphicsEffect(effect)
