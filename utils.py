# -*- coding: utf-8 -*-
"""
Utility class returning necessary data.
"""
from pathlib import Path
from setup import DependencyChecker
try:
    import pyseq
except ModuleNotFoundError:
    DependencyChecker().check_and_install_modules()
    import pyseq

class Utils:
    """
    Toolbox class containing various usefull methods.
    """
    def get_q_preset(self):
        """
        Provides a list of available quality preset.

        Returns:
            List of quality settings [str, str...].
        """
        return ['ultrafast','superfast','veryfast','faster','fast','medium','slow','slower','veryslow']

    def get_q_profile(self):
        """
        Provides a list of available quality profile.

        Returns:
            List of quality settings [str, str...].
        """
        return ['baseline','main','high','high10','high422','high444']

    def get_q_tunes(self):
        """
        Provides a list of available quality tunes.

        Returns:
            List of quality settings [str, str...].
        """
        return ['film','animation','grain','stillimage','fastdecode','zerolatency']

    def get_q_settings(self):
        """
        Provides a list of available quality settings.

        Returns:
            List of quality settings [str, str...].
        """
        return ['High','Mid','Low']

    def get_img_formats(self):
        return ['.jpg', '.png','.bmp','.tga','.tif','.webp','.exr']

    def get_all_exts(self):
        """
        Provides a list of supported media files extensions.

        Returns:
            List of supported media files extensions [str, str...].
        """
        return ['.mp4', '.ogv', '.gif', '.mpeg', '.m2v', '.mpg2', '.flc', '.mts', '.avs', '.movie', '.r3d', '.ogg', '.mxf', '.m2ts', '.webp', '.webm', '.divx', '.wmv', '.mov', '.mpg', '.vob', '.m2t', '.xvid', '.avi', '.flv', '.m4v', '.ts', '.dv', '.mv', '.mkv','.wav','.mp3','.aif'] + self.get_img_formats()

    def get_seqs_formated(self,folder):
        """
        Provides a list of image sequences in a ffmpeg-friendly format.

        Args:
            folder: path of a folder (str), cannot be None.

        Returns:
            List of image sequences [str, str...].
        """
        return [s.format('%h%p%t') for s in pyseq.get_sequences(folder) if s.length() > 1]

    def get_seqs(self,folder):
        """
        Provides a list of images sequences.

        Args:
            folder: path of a folder (str), cannot be None.

        Returns:
            List of image sequences [str, str...]
        """
        print(f"looking for sequences in folder {folder}")
        return [s for s in pyseq.get_sequences(folder) if s.length() > 1]
    
    def all_seqs(self,folder):
        """
        Provides a list of images sequences.

        Args:
            folder: path of a folder (str), cannot be None.

        Returns:
            List of image sequences [str, str...]
        """
        print(f"looking for sequences in folder {folder}")
        return [s for s in pyseq.Sequence(folder) ]

    def get_fast_options(self):
        """
        Provides a list of available audio codecs.

        Returns:
            List of audio codecs [str, str...]
        """
        #['X264 STANDARD', 'X264 LOW', 'X264 MID', 'X264 HIGH', 'X264 LOSSLESS', 'ProRes.4:2:2', 'ProRes.4:4:4', 'Media to Wav', 'Media to Mp3','Slideshow from images' ]
        return [t.stem for t in Path(__file__).parent.joinpath('presets').glob('*.json')]

    def get_audio_codecs(self):
        """
        Provides a list of available audio codecs.

        Returns:
            List of audio codecs [str, str...]
        """

        return ['aac', 'ac3', 'flac', 'opus', 'lib_fdk_aac', 'libmp3lame', 'pcm_s16le' ]

        '''
        return ['AAC', 'AAC+', 'AC-3', 'ADPCM 4X Movie', 'ADPCM G.722',
                 'ADPCM G.726', 'ADPCM IMA AMV', 'ADPCM ISS IMA', 'ADPCM Sound Blaster Pro 2-bit',
                 'AMR-NB', 'AMR-WB', 'Amazing Studio PAF Audio', 'Apple lossless audio', 'ATRAC1',
                 'DPCM Interplay', 'DPCM Sierra Online', 'DSP Group TrueSpeech', 'DST (Direct Stream Transfer)',
                 'DV audio', 'Enhanced AC-3', 'FLAC (Free Lossless Audio Codec)', 'Interplay ACM', 'Monkeys Audio',
                 'MP3 (MPEG audio layer 3)', 'MPEG-4 Audio Lossless Coding (ALS)', 'On2 AVC (Audio for Video Codec)',
                 'PCM A-law', 'RealAudio 1.0 (14.4K)', 'RealAudio Lossless', 'Sonic lossless', 'Vorbis', 'Windows Media Audio 1',
                 'Windows Media Audio Voice', 'Xbox Media Audio 1', 'Xbox Media Audio 2']
        '''

    def get_video_codecs(self):
        """
        Provides a list of available video codecs.

        Returns:
            List of video codecs [str, str...]
        """
        return ['libx264', 'libx264rgb', 'libx265', 'libxvid', 'png', 'cinepak', 'gif', 'hap', 'jpeg2000', 'libaom-av1', 'libtheora',
         'prores', 'libwebp' ]

